package com.frndzcode.preparationplus.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.frndzcode.preparationplus.R;

public class SigninActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        bindActivity();
    }

    private void bindActivity() {

    }

    public void signup(View view) {

    }

    public void signin(View view) {
    }
}
